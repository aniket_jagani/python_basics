# Numerical data types
# int
# float

print(type(6))
print(type(2-4))
print(type(2*8))
print(type(2/4))

# Other Operations
print(2**3)
print(10 // 4) # rounded version of division and it prints the quotient
print(3 % 4) # it prints the remainder for the division
