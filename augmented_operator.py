some_value = 50
some_value += 20
# += is an augmented operator
# It wont work if the variable has not been defined before hand
print(some_value)