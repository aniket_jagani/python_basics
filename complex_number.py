# There is another type for numbers that is complex number
# A complex number is created from real numbers. Python complex number can be created either using direct assignment statement or by using complex () function.
# Complex numbers which are mostly used where we are using two real numbers. For instance, an electric circuit which is defined by voltage(V) and current(C) are used in geometry, scientific calculations and calculus.
# complex([real[, imag]])
c = 3 + 6j
print(type(c))

# Also to see the binary version of a number we will use bin()
print(bin(5))
print(bin(88))
